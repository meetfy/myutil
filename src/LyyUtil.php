<?php


namespace meetlyy\Myutil;


class LyyUtil
{
    public function LYY_P($data)
    {
        echo '<pre>';
        if (is_array($data)) {
            var_dump(date('Y-m-d H:i:s ') . json_encode($data, JSON_UNESCAPED_UNICODE));
        } else {
            var_dump(date('Y-m-d H:i:s ') . (string)$data);
        }
        die;
    }
}